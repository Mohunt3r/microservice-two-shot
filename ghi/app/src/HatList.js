import React from 'react';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

const HatsList = () => {
  const [ hats, sethats ] = useState([])

  useEffect(()=> {
    const loadData = async () => {
      const url = 'http://localhost:8090/api/hats/';
      const response = await fetch(url);

      if (response.ok) {
        const data = await response.json();
        sethats(data.hat);
      } else {
        console.log("Error");
      }
    }

    loadData()

  }, [])

    return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Style</th>
          <th>Color</th>
          <th>Fabric</th>
          <th>Location</th>
        </tr>
      </thead>
        <tbody>
            {hats.map(hat=>
            <tr key={hat.href}>
                <td>{ hat.style_name }</td>
                <td>{ hat.color }</td>
                <td>{ hat.fabric }</td>
                <td>{ hat.location.closet_name }</td>
            </tr>
    )}
    </tbody>
    </table>
);
}

export default HatsList;
