import React from 'react';
import ShoesList from './ShoesList'
const c = {
	"shoes": [
		{
			"manufacturer": "Jordan",
			"model_name": "Jordan 1's",
			"color": "black/white",
			"picture_url": "https://quickshopiie.com/wp-content/uploads/2022/07/20220820-032734.jpg",
			"id": 1,
			"bin": "mikey's closet"
		},
		{
			"manufacturer": "Jordan",
			"model_name": "Jordan 1's",
			"color": "black/white",
			"picture_url": "https://quickshopiie.com/wp-content/uploads/2022/07/20220820-032734.jpg",
			"id": 2,
			"bin": "mikey's closet"
		}
	]
}
function MainPage() {
  return (
  <>
  <div>
    <ShoesList shoe = { c.shoes }/>
  </div>
  </>
        )
    // <div className="px-4 py-5 my-5 text-center">
    //   <h1 className="display-5 fw-bold">WARDROBIFY!</h1>
    //   <div className="col-lg-6 mx-auto">
    //     <p className="lead mb-4">
    //       Need to keep track of your shoes and hats? We have
    //       the solution for you!
    //     </p>
    //   </div>
    // </div>
  //);
}

export default MainPage;
