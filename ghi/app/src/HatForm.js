import React from 'react';

class HatForm extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            fabric: "",
			style_name: "",
			color: "",
			picture_url: "",
            location_id:"",
		    locations: [],
        };

        this.handleFabricChange = this.handleFabricChange.bind(this);
        this.handleStyleNameChange = this.handleStyleNameChange.bind(this);
        this.handleColorChange = this.handleColorChange.bind(this);
        this.handlePictureURLChange = this.handlePictureURLChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    // in here we import locations to select from when entering
    // a hat into tracking/storage
    async componentDidMount() {


        const url = 'http://localhost:8100/api/locations/';
        const response = await fetch(url);
        if (response.ok){
            const data = await response.json();
            this.setState({locations: data.locations});
        }
    }
// this will submit our completed form and clear it
    async handleSubmit(event) {
        //console.log("submit event detected")

        // keep the browser from interferring
        event.preventDefault();
        const data = {...this.state};
        data.fabric = data.fabric;
        data.style_name = data.style_name;
        data.color = data.color;
        data.picture_url = data.picture_url;
        delete data.locations;

        const getHatUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers:{
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(getHatUrl, fetchConfig)
        if (response.ok){
            const newHat = await response.json();
            console.log(newHat);

            const cleared = {
                color: '',
                fabric: '',
                style_name: '',
                picture_url: '',
                location: '',

            };
            this.setState(cleared);
        }

    }
    handleFabricChange(event) {
        const value = event.target.value;
        this.setState({fabric: value});
    }

    handleStyleNameChange(event) {
        const value = event.target.value;
        this.setState({style_name: value});
    }

    handleColorChange(event) {
        const value = event.target.value;
        this.setState({color: value});
    }

    handlePictureURLChange(event) {
        const value = event.target.value;
        this.setState({picture_url: value});
    }

    handleLocationChange(event) {
        const value = event.target.value;
        this.setState({location_id: value});
    }




    render(){
        return(
            <div className="my-5 container">
            <div className="column">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new hat</h1>
                    <form onSubmit={this.handleSubmit} id="create-hat-form">
                    <div className="form-floating mb-3">
                        <input onChange={this.handleFabricChange} value={this.state.fabric} placeholder="Fabric" required type="text" name ="fabric" id="fabric" className="form-control"/>
                        <label htmlFor="name">Fabric</label>
                    </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleColorChange} value={this.state.color} placeholder="Color" required type="text" name ="color" id="color" className="form-control"/>
                    <label htmlFor="name">Color</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleStyleNameChange} value={this.state.style_name} placeholder="Style Name" required type="text" name ="styleName" id="styleName" className="form-control"/>
                    <label htmlFor="name">Style Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handlePictureURLChange} value={this.state.picture_url} placeholder="Picture URL" required type="url" name ="pictureURL" id="pictureURL" className="form-control"/>
                    <label htmlFor="name">Picture URL</label>
                  </div>
                  <div className="mb-3">
                    <select onChange={this.handleLocationChange} value={this.state.location} required name="locations" id='locations' className='form-select'>
                    <option value="">Choose a location</option>
                      {this.state.locations.map(location => {
                       return (
                        <option key={location.href} value={location.id}>
                          {location.closet_name}
                        </option>
                      );
                     })}
                    </select>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
          </div>

        )
    };

}
export default HatForm;
