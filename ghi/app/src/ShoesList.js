import React from 'react';
function ShoesList({shoe}) {
    console.log(shoe)
    return (
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Manufacturer</th>
              <th>Model name</th>
              <th>Color</th>
              <th>Picture</th>
              <th>Bin</th>
            </tr>
          </thead>
          <tbody>
            {shoe.map(shoes => {
              return (
                <tr key={shoes.href}>
                  <td>{ shoes.manufacturer }</td>
                  <td>{ shoes.model_name }</td>
                  <td>{ shoes.color }</td>
                  <img src={shoe.picture_url}></img>
                  <td>{ shoes.bin }</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      );
    }

export default ShoesList;
