import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hats_project.settings")
django.setup()

# Import models from hats_rest, here.
from hats_rest import models
# from hats_rest.models import Something
from hats_rest.models import LocationVO

def get_locations():

    # 8100 is the port the wardrobe api is running in docker that the broswers
    # see through localhost:8000 , but another running microservice doesn't and can't access
    # another service that way, so we give it the following network address:
    # 'wardwrobe-api' comes from the docker servicename
    # we also have to add 'wardrobe-api' to list of allowed django hosts
    response = requests.get("http://wardrobe-api:8000/api/locations/")

    content = json.loads(response.content)
    for location in content['locations']:
        LocationVO.objects.update_or_create(
            import_href=location['href'],
            closet_name=location['closet_name'],
            section_number=location['section_number'],
            shelf_number=location['shelf_number'],
        )

def poll():
    while True:
        # print('Hats poller polling for data')
        try:
            # Write your polling logic, here
            get_locations()
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(10)


if __name__ == "__main__":
    poll()
