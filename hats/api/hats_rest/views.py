from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from .models import Hat, LocationVO
import json
from django.http import JsonResponse
from common.json import ModelEncoder

# Create your views here.
class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ["closet_name", "section_number", "shelf_number", "id"]

class HatsListEncoder(ModelEncoder):
    model = Hat
    properties = ["fabric", "style_name", "color", "picture_url", "id"]

    def get_extra_data(self, o):
        return {"location": o.location.closet_name}

@require_http_methods(['GET', 'POST'])
def api_list_hats(request):

    if request.method == 'GET':
        Hats = Hat.objects.all()
        return JsonResponse(
            {"hats": Hats},
            encoder=HatsListEncoder,
        )

    else:
        content = json.loads(request.body)

        try: # test and set location if location exists in database
             # getting the numeric location ID number from the response content
            location_id = content['location_id']
             # set location equal to the locationVO object that will match with the id
            location = LocationVO.objects.get(id=location_id)
             # sets content location equal to the location
            content['location'] = location

        except LocationVO.DoesNotExist:
            return JsonResponse(
                {'message': 'Location does not exist'},
                status = 400
            )

        # if the above try was successful, create a new hat instance
        hat = Hat.objects.create(**content)

        return JsonResponse(
            hat,
            encoder = HatsListEncoder,
            safe = False,
        )

@require_http_methods(['GET', 'DELETE'])
def api_list_hat(request, pk):
    hat = Hat.objects.get(id=pk)
    if request.method == 'GET':
        return JsonResponse(
            hat,
            encoder = HatsListEncoder,
            safe = False,
        )
    else: #DELETE
        try:
            hat = Hat.objects.get(id=pk)
            hat.delete()
            return JsonResponse(
                {'message': 'Hat was deleted successfully'},
                safe = False,
            )
        except Hat.DoesNotExist:
            return JsonResponse({"message": 'The hat you are trying to delete does not exist'})
